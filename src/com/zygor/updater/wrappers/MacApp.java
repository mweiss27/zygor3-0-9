package com.zygor.updater.wrappers;

import java.awt.Desktop;
import java.awt.Taskbar;
import java.awt.desktop.AppReopenedEvent;
import java.awt.desktop.AppReopenedListener;
import java.awt.desktop.QuitEvent;
import java.awt.desktop.QuitHandler;
import java.awt.desktop.QuitResponse;

import com.zygor.updater.mvc.client.ClientController;
import com.zygor.updater.resources.LocalResource;


public class MacApp {

	private static Desktop desktop = Desktop.getDesktop();
	private static Taskbar taskbar = Taskbar.getTaskbar();

	public static void initialize() {
		taskbar.setIconImage(LocalResource.ICON.getImage());
		desktop.addAppEventListener(new AppReopenedListener() {
			
			@Override
			public void appReopened(AppReopenedEvent arg0) {
				try {
					ClientController.instance().view.setVisible(true);
				} catch (Exception ignored) {}
			}
		});
		
		desktop.setQuitHandler(new QuitHandler() {
			
			@Override
			public void handleQuitRequestWith(QuitEvent arg0, QuitResponse arg1) {
				System.exit(0);
			}
		});
	}

	public static void requestForeground(boolean all) {
		desktop.requestForeground(true);
	}

	public static void requestUserAttention(boolean enabled) {
		taskbar.requestUserAttention(enabled, false);
	}

}
